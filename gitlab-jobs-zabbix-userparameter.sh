!/bin/bash

# 1 tier of script - declaration of variables such as current date current time and time deduction from it (105).
##finding recent active gitlab projects and entering them into a file.
##reading from projects jobs file line by line. using while loops where $ i is the project id.
###using the jq utility that formats the api output to a friendly json form.

currentH=`date +%H%M`
data=`date +"%Y-%m-%d"`
token=yourtoken
count=$(expr $currentH - 105)

curl -s --globoff --header "PRIVATE-TOKEN: $token" 'https://gitlab.example.com.pl/api/v4/projects??per_page=100archived=false&&order_by=last_activity_at' | jq '.[].id' > /tmp/projects


while read -r i

        do

`curl -s --globoff --header "PRIVATE-TOKEN: $token" 'https://gitlab.example.com.pl/api/v4/projects/'$i'/jobs?scope[]=failed' | jq '.[] | {created_at }'  >>/tmp/fails 2>/dev/null`
`curl -s --globoff --header "PRIVATE-TOKEN: $token" 'https://gitlab.example.com.pl/api/v4/projects/'$i'/jobs?scope[]=pending' | jq '.[] | {created_at }'  >>/tmp/pending 2>/dev/null`
`curl -s --globoff --header "PRIVATE-TOKEN: $token" 'https://gitlab.example.com.pl/api/v4/projects/'$i'/jobs?scope[]=canceled' | jq '.[] | {created_at }'  >>/tmp/canceled 2>/dev/null`
`curl -s --globoff --header "PRIVATE-TOKEN: $token" 'https://gitlab.example.com.pl/api/v4/projects/'$i'/jobs?scope[]=success' | jq '.[] | {created_at }'  >>/tmp/success 2>/dev/null`
#`curl -s --globoff --header "PRIVATE-TOKEN: $token" 'https://gitlab.example.com.pl/api/v4/projects/'$i'/jobs?scope[]=running' | jq '.[] | {created_at }'  >>/tmp/jobs 2>/dev/null`

        done < "/tmp/projects"

# 2 tier of script - formatting the time to a decimal gitapi-friendly form 

failcmd=$(cat /tmp/fails | grep $data | cut -f2 -d"T" | cut -f1,2 -d":" | sed 's/://g' | sort)
succescmd=$(cat /tmp/success | grep $data | cut -f2 -d"T" | cut -f1,2 -d":" | sed 's/://g' | sort)
pendingcmd=$(cat /tmp/pending | grep $data | cut -f2 -d"T" | cut -f1,2 -d":" | sed 's/://g' | sort) 
canceledcmd=$(cat /tmp/canceled | grep $data | cut -f2 -d"T" | cut -f1,2 -d":" | sed 's/://g' | sort)

# ge = less then jesli znajdzie wynik mniejszy to wylistuje je jesli nie to 0.

### 3 tier of script - checking the formatted time line by line to compare it with the entered variable count (currentH - 105).
###Redirecting the output of commands from tier 2 to the loop. thanks to that we don't have to create additional files

fails () {
i=0
while read -r f

        do
[ "$f" -ge "$count" ] && ((i=i+1)) 

done <<< "$failcmd"

echo $i > /tmp/fail-result

}

success () {

i=0
while read -r s

        do
[ "$s" -ge "$count" ] && ((i=i+1)) 

done <<< "$succescmd"

echo $i > /tmp/success-result

}

pending () {

i=0
while read -r p

        do
[ "$p" -ge "$count" ] && ((i=i+1))

done <<< "$pendingcmd"

echo $i > /tmp/pending-result

}

canceled () {

i=0
while read -r d

        do
[ "$d" -ge "$count" ] && ((i=i+1)) 

done <<< "$canceledcmd"

echo $i > /tmp/canceled-result

}

success
fails
pending
canceled

## remove all files with jobs 
rm -f /tmp/fails /tmp/success /tmp/pending /tmp/canceled
