### potrzebujemy instalacji narzedznia datree curl https://get.datree.io | /bin/bash

##uzycie bash vuln_scan_k8s.sh deployment example-namespace
##https://gitlab.com/matixooo/bash-projects/-/raw/master/vuln_scan_k8s.sh



#!/bin/bash 


krewdir=~/.krew

if [ -d "$krewdir" ]; then
  echo "$krewdir does exist. Krew plugin is installed"
  export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
  kubectl krew install neat

else

echo "$krewdir doesnt exists, installing..."

(
  set -x; cd "$(mktemp -d)" &&
  OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
  ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
  KREW="krew-${OS}_${ARCH}" &&
  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
  tar zxvf "${KREW}.tar.gz" &&
  ./"${KREW}" install krew
)

export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

kubectl krew install neat

fi

# create temporary file
temp_dir=$(mktemp -d)

list=$(kubectl get $1 -n $2 | awk 'NR>1{print $1}')
readarray  arr <<<  $list

echo -e "\nScanning $1s with datree  :\n"
for i in ${arr[@]}
do 


kubectl get $1 $i -n $2 -o yaml | kubectl neat > $temp_dir/$i

done

datree test $temp_dir/* &> vuln_raport-Kind:"$1"Ns:"$2"


rm -R ${temp_dir}

echo " scanning cluster vulnaribilities"

kubectl apply -f https://raw.githubusercontent.com/aquasecurity/kube-bench/main/job.yaml 

bench=$(kubectl get pods -A --no-headers  | awk 'NR>1{print $2}' | grep bench)

echo " Check raport in kubernetes cluster -- kubectl logs $bench "
