# $1 git branch name

cd ./repo-dir

COMMIT_COMM='Commit for rev. $1 done at `date`'

git add . &&

git checkout $1 &

git commit -m "$COMMIT_COMM"

git push origin $1

